import React, {useState, useEffect} from 'react';
import axios from "axios";
import {Collapse, Button, Modal, ModalHeader, ModalBody, ModalFooter, Spinner} from 'reactstrap';
import {toast} from "react-toastify";


function API(props) {

    const [loader, setLoader] = useState(false);
    const [posts, setPosts] = useState([]);
    const [error, setError] = useState(null);
    const [tempId, setTempId] = useState(null);
    const [comments, setComments] = useState([]);

    const [modal, setModal] = useState(false);


    const toggle = id => {

        setTempId(tempId === id ? null : id)

        axios.get('https://jsonplaceholder.typicode.com/photos?albumId=' + id).then(response => {
            setComments(response.data)
        }).catch(error => {
            setError(error)
        })
        setModal(!modal)
    }

    console.log(comments)

    useEffect(() => {
        fetchPosts()
    }, [])

    const fetchPosts = () => {

        setLoader(true)
        axios.get('https://jsonplaceholder.typicode.com/albums').then(response => {
            setPosts(response.data)
            setLoader(false)
        }).catch(error => {
            setError(error)
            setLoader(false)
        })
    }
    console.log(posts)
    return (

        <section className="section-api py-lg-5">
            {error ? <React.StrictMode>{toast.error('Serverda xatolik')}</React.StrictMode> : null}
            <div className="container">
                <h2>API</h2>
                <div className="row">
                    {console.log(posts)}
                    {loader ?
                        <div className="col-lg-2 offset-5 text-center">
                            <Spinner color="dark"/>
                        </div>
                        :
                        posts.length && posts.map((item, index) => (

                            <div key={index} className="col-lg-3 mb-lg-3">
                                <div className="card h-100">
                                    <div className="card-body">
                                        <h5>{index + 1}. {item.title}</h5>
                                    </div>
                                    <div className="card-footer text-center">
                                        <Button color="danger" onClick={() => toggle(item.id)}>
                                            PhotoAlbum
                                        </Button>
                                    </div>
                                </div>

                            </div>
                        ))
                    }
                </div>
            </div>
            <Modal isOpen={modal} toggle={() => setModal(!modal)} className="modal-xl">
                <ModalHeader toggle={() => setModal(!modal)}>Photos</ModalHeader>
                <ModalBody>
                    <div className="col-lg-12">
                        <div className="row">
                            {comments.length && comments.map((item, index) => (
                                <div key={index} className="col-lg-3 mb-lg-3">
                                    <div className="card h-100">
                                        <div className="card-header text-center">
                                            <h5>{index + 1}-photo</h5>
                                            <img src={item.thumbnailUrl} alt="rasm"/>
                                        </div>
                                        <div className="card-body">
                                            <h6>{item.title}</h6>
                                        </div>

                                    </div>
                                </div>
                            ))}
                        </div>

                    </div>
                </ModalBody>
                <ModalFooter>
                    <Button color="secondary" onClick={() => setModal(!modal)}>Cancel</Button>
                </ModalFooter>
            </Modal>

        </section>
    )
}

export default API