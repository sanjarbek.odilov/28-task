import React, {Component} from 'react';
import {Modal, ModalHeader, ModalBody, ModalFooter, Button, ButtonGroup} from "reactstrap";

class Product extends Component {

    constructor() {
        super();
        this.state = {
            content: null,
            modal: false,
            tempObject: null,

        }
    }

    componentDidMount() {
        this.getProductsList()
    }

    getProductsList = () => {

        const {data} = this.props


        this.setState({
            content: data.length ? data.map((item, index) => (
                    <div key={index} className="col-lg-3">
                        <div className="card">
                            <img className="card-img-top" src={item.imgURL} alt="Card image cap"/>
                            <div className="card-body">
                                <h5 className="card-title">{item.name}</h5>
                                <h5 className="card-text">{item.brand}</h5>
                                <p className="card-text"><b>O'lchami: </b>{item.size}</p>
                                <p className="card-text"><b>Narxi: </b>{item.price} $</p>
                            </div>
                            <div className="card-header">
                                <button onClick={() => this.toggle(item)} className="btn btn-primary">Add to card</button>
                            </div>
                        </div>
                    </div>
                )) :
                <div className="d-flex justify-content-center w-100 my-5">
                    <div className="spinner-border" role="status">
                        <span className="sr-only">Loading...</span>
                    </div>
                </div>
        })
    }

    toggle = (item) => {
        this.setState({
            modal: !this.state.modal,
            tempObject: item ? item : null,
        })
    }

    render() {

        const {content, modal, tempObject} = this.state
        console.log(tempObject)

        const {changeCount, tempPrice, tempCount, updateData, addToCart} = this.props

        return (
            <section className="products-section">
                <div className="container">
                    <h1>Product page</h1>
                    <div className="row">
                        {content}
                    </div>
                </div>

                <Modal isOpen={modal} toggle={() => {
                    updateData();
                    this.toggle();
                }} className="modal-xl">
                    <ModalHeader toggle={() => {
                        updateData();
                        this.toggle();
                    }}>Modal title</ModalHeader>
                    <ModalBody>
                        <div className="container">
                            <div className="row">
                                <div className="col-lg-4">
                                    <div className="card h-100">
                                        <div className="card-header bg-transparent">
                                            <h4 className="card-title">Info</h4>
                                        </div>
                                        <div className="card-body">
                                            <h5>{tempObject && tempObject.name}, {tempObject && tempObject.brand}</h5>
                                            <h6>size: {tempObject && tempObject.size}</h6>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-lg-4">
                                    <div className="card h-100">
                                        <div className="card-header bg-transparent">
                                            <h4 className="card-title">Product</h4>
                                        </div>
                                        <div className="card-body p-0">
                                            <img className="w-100" src={tempObject && tempObject.imgURL}
                                                 alt="product-img"/>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-lg-4">
                                    <div className="card h-100">
                                        <div className="card-header bg-transparent">
                                            <h4 className="card-title">Action</h4>
                                        </div>
                                        <div className="card-body">
                                            <h6>Price: {tempObject && tempObject.price}$</h6>
                                            {tempPrice ? <h6>Total price: {tempPrice}$</h6> : null}
                                            <h6>Total count: {tempCount}</h6>
                                            <ButtonGroup>
                                                <Button onClick={() => changeCount(tempObject, false)}>-</Button>
                                                <Button outline color="secondary" disabled>{tempCount}</Button>
                                                <Button onClick={() => changeCount(tempObject, true)}>+</Button>
                                            </ButtonGroup>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </ModalBody>
                    <ModalFooter>
                        <Button color="primary" onClick={() => { updateData();
                            addToCart(tempObject, this.setState({modal: false}));
                        }}>Submit</Button>{' '}
                        <Button color="secondary" onClick={() => {
                            updateData();
                            this.toggle();
                        }}>Cancel</Button>
                    </ModalFooter>
                </Modal>
            </section>
        );
    }
}

export default Product;