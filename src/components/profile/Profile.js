import {Button, Card, CardBody, CardHeader, Col, Nav, NavItem, NavLink, Row, TabContent, TabPane} from "reactstrap";
import React, {useState} from 'react';


export default function Profile(props) {

    const [activeTab, setActiveTab] = useState('1');
    const {history, deleteProductFromCart} = props

    const toggle = tab => {
        if (activeTab !== tab) setActiveTab(tab);
    }

    return (
        <section className="profile">
            <div className="container my-5">
                <div className="d-flex justify-content-between">
                    <h1 className="mb-5">My profile</h1>
                    <div className="d-flex align-items-center"></div>
                </div>

                <Nav tabs>
                    <NavItem>
                        <NavLink
                            className={({active: activeTab === '1'})}
                            onClick={() => {
                                toggle('1');
                            }}
                        >
                            Settings
                        </NavLink>
                    </NavItem>
                    <NavItem>
                        <NavLink
                            className={({active: activeTab === '2'})}
                            onClick={() => {
                                toggle('2');
                            }}
                        >
                            History
                        </NavLink>
                    </NavItem>
                </Nav>

                <TabContent activeTab={activeTab}>
                    <TabPane tabId="1">
                        <Row>
                            <Col sm="12" className="col-lg-4 my-4">
                                <h4>My profile</h4>
                                <Card>
                                    <CardHeader>
                                        <h4>Jackie Chan</h4>
                                    </CardHeader>
                                    <CardBody>
                                        Info
                                    </CardBody>
                                </Card>

                            </Col>
                        </Row>
                    </TabPane>
                    <TabPane tabId="2">


                        <div className="row mt-5">
                            {history.length ? history.map((item, index) => (
                                    <div key={index} className="col-lg-6">
                                        <div className="row">
                                            <div className="col-lg-6">
                                                <div className="card border-0">
                                                    <img className="w-100" src={item.imgURL} alt="product"/>
                                                </div>
                                            </div>
                                            <div className="col-lg-6">
                                                <div className="card border-0">
                                                    <h5>{item.name}, {item.brand}</h5>
                                                    <h5>Size: {item.size}</h5>
                                                    <h5>Price: <span className="badge badge-danger">{item.price}$</span>
                                                    </h5>
                                                    <h5>Total count: <span
                                                        className="badge badge-warning">{item.totalCount}</span></h5>
                                                    <h5>Total price: <span
                                                        className="badge badge-danger">{item.totalPrice}$</span></h5>
                                                </div>
                                                <div className="card border-0">
                                                    <Button color="danger"
                                                            onClick={() => deleteProductFromCart(item.id)}>Remove</Button>
                                                </div>
                                            </div>
                                            <div className="col-lg-12">
                                                <hr/>
                                            </div>
                                        </div>
                                    </div>
                                )) :
                                <div className="col-lg-2 offset-5">
                                    <div className="card border-0">
                                        <h2>No data</h2>
                                    </div>
                                </div>
                            }
                        </div>


                    </TabPane>
                </TabContent>
            </div>
        </section>
    )
}
